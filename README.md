# Getting started with FDSN Webservice library

With this library you can get information about quakes, getting data from FDSN webservices.

You can search quakes filtering by:
- Event ID (int unique event -earthquake- identifier)
- DateTime range (Datetime min -> Datetime max)
- Geographical selection - BoundingBox (Lat/Lon min -> Lat/Lon max)
- Geographical selection - Radius (Lat/Lon + Radius min -> max)
- Magnitude range (Magnitude min -> Magnitude max)
- Depth range (Depth min -> Depth max)

except search by *Event ID*, you can combine all of them.

The library returns the list of quakes found and all their details.

## Install and usage

See wiki about Install, Autoload and Usage here: https://gitlab.rm.ingv.it/fdsn/webservices-library/-/wikis/Home

### Note 
During object __construct__ a method checks if FDSN webservice version is compatible with the library, if unsupported a __RuntimeException__ is raised.

## Documentation
You can find documentation, automatically created by PHPDocumentor here: http://fdsn.gitpages.rm.ingv.it/webservices-library/

## Test and Deploy
Tests are realized using PHPUnit.


## Contributing
If you want to contribute, please use pull requests.
To get a best integration, please code using Behavior Driven Development (BDD), Test Driven Development (TDD) and Calisthenic Programming.

## Authors and acknowledgment
Diego Sorrentino, Istituto Nazionale di Geofisica e Vulcanologia, https://www.ingv.it/organizzazione/chi-siamo/personale/#922

## License
GPL v3

## Project status
Development in progress
