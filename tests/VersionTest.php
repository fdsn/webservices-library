<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Fdsn\Webservices\Version as Fdsnws_Version;
use Fdsn\Webservices\Settings as Fdsnws_Settings;


class VersionTest extends TestCase{
	private $obj; 

	public function testVersionByFDQN(): void {
		$this->obj= new Fdsnws_Version('webservices.ms.ingv.it');

		$this->assertMatchesRegularExpression('/^' . Fdsnws_Settings::fdsnSupportedVersion . '/', $this->obj->version());
	}


}
