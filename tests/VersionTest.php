<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Fdsn\Webservices\Version as Fdsnws_Version;


class VersionTest extends TestCase{
	private $obj; 

	public function testVersionByFDQN(): void {
		$this->obj= new Fdsnws_Version('webservices.ms.ingv.it');

		$this->assertSame('1.60.0', $this->obj->version());
	}


}
