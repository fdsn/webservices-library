<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Fdsn\Webservices\Catalogs as Fdsnws_Catalogs;


class CatalogsTest extends TestCase{
	private $obj; 

	public function testCatalogs(): void {
		$this->obj= new Fdsnws_Catalogs('webservices.ms.ingv.it');
		$this->assertGreaterThan( 0, $this->obj->fetch() );
		$this->assertGreaterThan( 0, $this->obj->getNumRows() ); //same results of fetch

		$this->assertIsString($this->obj->rawXMLCatalogs());
		$this->assertNotEmpty($this->obj->rawXMLCatalogs(), 'Catalogs is empty');

		$this->assertInstanceOf("\\DOMDocument", $this->obj->DOMCatalogs());

		foreach($this->obj as $catalogObj)
			$this->assertInstanceOf("\\Fdsn\\DataStructure\\Catalog", $catalogObj);
	}



}
