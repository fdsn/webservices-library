<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Fdsn\Webservices\Contributors as Fdsnws_Contributors;


class ContributorsTest extends TestCase{
	private $obj; 

	public function testContributors(): void {
		$this->obj= new Fdsnws_Contributors('webservices.ms.ingv.it');
		$this->assertGreaterThan(0, $this->obj->fetch());	
		$this->assertGreaterThan(0, $this->obj->getNumRows());	

		$this->assertIsString($this->obj->rawXMLContributors());
		$this->assertNotEmpty($this->obj->rawXMLContributors(), 'Contributors is empty');

		$this->assertInstanceOf("\\DOMDocument", $this->obj->DOMContributors());


		foreach($this->obj as $contributorObj)
			$this->assertInstanceOf("\\Fdsn\\DataStructure\\Contributor", $contributorObj);
	}
}
