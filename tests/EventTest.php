<?php 
declare(strict_types=1);
require_once __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

use Fdsn\Webservices\Event as Fdsnws_Event;

use Fdsn\DataStructure\Author;
use Fdsn\DataStructure\DateTimeRange;
use Fdsn\DataStructure\Depth;
use Fdsn\DataStructure\DepthRange;
use Fdsn\DataStructure\Epicenter;
use Fdsn\DataStructure\Istat;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\LatLonRange;
use Fdsn\DataStructure\Location;
use Fdsn\DataStructure\Magnitude;
use Fdsn\DataStructure\MagnitudeRange;
use Fdsn\DataStructure\Province;
use Fdsn\DataStructure\Quake as DS_Quake;
use Fdsn\DataStructure\Radius;
use Fdsn\DataStructure\RadiusRange;

class EventTest extends TestCase{
	private $obj; 

	public function testFilterByEventId(): void {
		$eventId = 8863681;
		
		$this->obj= new Fdsnws_Event('text', 'my-app', 'webservices.ms.ingv.it');

		$this->obj->addFilterByEventId($eventId);
		$this->obj->fetch();

		$this->assertSame(1, $this->obj->getNumRows());
		$this->assertSame($eventId, $this->obj->getSingleElemFound()->eventId());
	}

	public function testFilterByDateTimeRange(): void {
		$this->obj= new Fdsnws_Event('text', 'my-app', 'webservices.ms.ingv.it');

		$this->obj->addFilterByDateTimeRange( 
				new DateTimeRange( 
					new \DateTime('2009-04-06T03:00:00'),
					new \DateTime('2009-04-06T04:00:00'),
				)
			);
		$this->obj->fetch();

		$this->assertSame(35, $this->obj->getNumRows());
	}

	public function testFilterByDateTimeRangeAndBoundingBox(): void {
		$this->obj= new Fdsnws_Event('text', 'my-app', 'webservices.ms.ingv.it');

		$this->obj->addFilterByDateTimeRange( 
				new DateTimeRange( 
					new \DateTime('2009-04-06T03:00:00'),
					new \DateTime('2009-04-06T04:00:00'),
				)
			);

		$this->obj->addFilterByBoundingBox(
				new LatLonRange( 
					new LatLon(42, 13),
					new LatLon(43, 14)
				)
			);

		$this->obj->fetch();

		$this->assertSame(34, $this->obj->getNumRows());
	}

	public function testFilterByDateTimeRangeAndMagnitudeRange(): void {
		$this->obj= new Fdsnws_Event('text', 'my-app', 'webservices.ms.ingv.it');

		$this->obj->addFilterByDateTimeRange( 
				new DateTimeRange( 
					new \DateTime('2009-04-06T03:00:00'),
					new \DateTime('2009-04-06T04:00:00'),
				)
			);

		$this->obj->addFilterByMagnitudeRange(
				new MagnitudeRange( 
					new Magnitude('ml', 3),
					new Magnitude('ml', 5),
				)
			);

		$this->obj->fetch();

		$this->assertSame(11, $this->obj->getNumRows());
	}

	public function testFilterByDateTimeRangeAndDepthRange(): void {
		$this->obj= new Fdsnws_Event('text', 'my-app', 'webservices.ms.ingv.it');

		$this->obj->addFilterByDateTimeRange( 
				new DateTimeRange( 
					new \DateTime('2009-04-06T03:00:00'),
					new \DateTime('2009-04-06T04:00:00'),
				)
			);

		$this->obj->addFilterByDepthRange(
				new DepthRange( 
					new Depth(0),
					new Depth(100)
				)
			);

		$this->obj->fetch();

		$this->assertSame(35, $this->obj->getNumRows());
	}

	public function testFilterByDateTimeRangeAndRadiusSearch(): void {
		$this->obj= new Fdsnws_Event('text', 'my-app', 'webservices.ms.ingv.it');

		$this->obj->addFilterByDateTimeRange( 
				new DateTimeRange( 
					new \DateTime('2009-04-06T03:00:00'),
					new \DateTime('2009-04-06T04:00:00'),
				)
			);

		$this->obj->addFilterByRadiusRange(
				new LatLon( 42.3, 13.4),
				new RadiusRange (
					new Radius(0),
					new Radius(5)
				)
			);

		$this->obj->fetch();

		$this->assertSame(35, $this->obj->getNumRows());
	}
}
