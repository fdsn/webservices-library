<?php
namespace Fdsn\Webservices;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

use Fdsn\DataStructure\Contributor as DS_Contributor;

/**
 * PHP library to access FDSN Webservices and request Contributors
 *
 * @param ?string 	$fdns_server	Fdns webservice domain name (default: webservices.ms.ingv.it)
 */
class Contributors implements IteratorAggregate {
	const fdsnSupportedVersion = '^1.60';

	private string $fdns_server;
	private string $webserviceFullPath;

	private string $url;

	private string $contributorsXMLString;
	private \DOMDocument $contributorsDOM;
	private array $contributorsArray;
	private int $counter = 0;

	function __construct( ?string $fdns_server){

		$this->fdnsServer =  empty($fdns_server) ? Settings::defaultFdsnServer : $fdns_server;

		$this->webserviceFullPath = sprintf(Settings::basePath,
			$this->fdnsServer
			);

		if( ! $this->fdsnSupportedVersion())
			throw new \RuntimeException("Unsupported FDSN version");

		if ( '' == $this->retrieveContributors() )
			throw new \ValueError('Contributors is empty');

		$this->parseContributors();
	}
	
	/**
	 * Return the number of elems found
	 * @return int 		The number of elems found
	 */
	public function getNumRows():int { return $this->counter; }

	/**
	 * Iterate over contributors found
	 * @return array 	array of \Fdsn\DataStructure\Contributor obj
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->contributorsArray); }

	/**
	 * Get RAW Contributors XML String
	 * @return string	Get RAW Contributors XML String
	 */
        public function rawXMLContributors():string { return $this->contributorsXMLString; }

	/**
	 * Get Contributors parsed in a DOM Document
	 * @return \DOMDocument Get Contributors parsed in a DOMDocument
	 */
        public function DOMContributors():\DOMDocument{ return $this->contributorsDOM; }

	/**
	 * Check if FDSN server version is supported
	 */
	private function fdsnSupportedVersion():bool{
		$version = new Version($this->fdnsServer);
                return preg_match('/' . self::fdsnSupportedVersion . '/', $version->version());

	}

	/**
	 * Connect to FDSN server to retrieve working version
	 * @return string FDSN running version found (trimmed by newline)
	 */
	private function retrieveContributors():string {
		$contributorsUrl = $this->webserviceFullPath . '/contributors';

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $contributorsUrl,
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> 1,
					CURLOPT_TIMEOUT		=> 60
					)
				);
		$this->contributorsXMLString = trim(curl_exec($curlSession));
		curl_close($curlSession);

		return $this->contributorsXMLString;
	}

	/**
	 * Parse XML Contributors and store it into a DOM Document AND all values found into an Iterable array
	 */
	private function parseContributors():void{
		$this->contributorsDOM = new \DOMDocument('1.0', 'UTF-8');
		$this->contributorsDOM->loadXML($this->contributorsXMLString);

		$contributors = $this->contributorsDOM->getElementsByTagName('Contributor');
		foreach($contributors as $name)
			$this->contributorsArray[] = new DS_Contributor(trim($name->nodeValue));
	
		$this->counter = count($this->contributorsArray);
	}

	
}
?>
