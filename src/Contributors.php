<?php
namespace Fdsn\Webservices;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

use Fdsn\DataStructure\Contributor as DS_Contributor;

/**
 * PHP library to access FDSN Webservices and request Contributors
 *
 * @param ?string 	$fdsn_server	Fdns webservice domain name (default: webservices.ms.ingv.it)
 */
class Contributors implements IteratorAggregate {
	private string $fdsn_server;
	private string $webserviceFullPath;

	private string $url;

	private string $contributorsXMLString;
	private \DOMDocument $contributorsDOM;
	private array $contributorsArray;
	private int $counter = 0;

	function __construct( ?string $fdsn_server){

		$this->fdsnServer =  empty($fdsn_server) ? Settings::defaultFdsnServer : $fdsn_server;

		$this->webserviceFullPath = sprintf(Settings::basePath,
			$this->fdsnServer
			);

		if( ! $this->fdsnSupportedVersion())
			throw new \RuntimeException("Unsupported FDSN version");
	}
	
	/**
	 * Return the number of elems found
	 * @return int 		The number of elems found
	 */
	public function getNumRows():int { return $this->counter; }

	/**
	 * Iterate over contributors found
	 * @return array 	array of \Fdsn\DataStructure\Contributor obj
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->contributorsArray); }

	/**
	 * Get RAW Contributors XML String
	 * @return string	Get RAW Contributors XML String
	 */
        public function rawXMLContributors():string { return $this->contributorsXMLString; }

	/**
	 * Get Contributors parsed in a DOM Document
	 * @return \DOMDocument Get Contributors parsed in a DOMDocument
	 */
        public function DOMContributors():\DOMDocument{ return $this->contributorsDOM; }

	/**
	 * Check if FDSN server version is supported
	 */
	private function fdsnSupportedVersion():bool{
		$version = new Version($this->fdsnServer);
                return preg_match('/' . Settings::fdsnSupportedVersion . '/', $version->version());

	}

	/**
	 * Fetch contributors (xml document) from selected FDSN server
	 * 
	 * @return int number of catalogs found
	 */
	public function fetch():int {
		$contributorsUrl = $this->webserviceFullPath . '/contributors';

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $contributorsUrl,
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> 1,
					CURLOPT_TIMEOUT		=> 60
					)
				);
		$xmlResponse = trim(curl_exec($curlSession));
		curl_close($curlSession);

		$this->contributorsXMLString = ( $this->parse($xmlResponse) > 0 ) ? $xmlResponse : '';

		return $this->counter;
	}

	/**
	 * Parse XML Contributors and store it into a DOM Document AND all values found into an Iterable array
	 * 
	 * @return int number of contributors found
	 */
	private function parse(string $xmlResponse):int{
		$this->contributorsDOM = new \DOMDocument('1.0', 'UTF-8');
		$this->contributorsDOM->loadXML($xmlResponse);

		$contributors = $this->contributorsDOM->getElementsByTagName('Contributor');
		foreach($contributors as $name)
			$this->contributorsArray[] = new DS_Contributor(trim($name->nodeValue));
	
		$this->counter = count($this->contributorsArray);

		return $this->counter;
	}

	
}
?>
