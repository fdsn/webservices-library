<?php
namespace Fdsn\Webservices;

/**
 * FDSN Settings
 */
class Settings {
	/**
	 * @var Base URL to compile with FDSN server url 
	 */ 
	public const basePath = 'https://%s/fdsnws/event/1';

	/**
	 * @var Default server url
	 */ 
	public const defaultFdsnServer  = 'webservices.ms.ingv.it';

	/**
	 * @var Max supported version
	 */ 
	public const fdsnSupportedVersion = '^1.6';
}
?>
