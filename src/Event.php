<?php
namespace Fdsn\Webservices;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

use Fdsn\DataStructure\Author;
use Fdsn\DataStructure\DateTimeRange;
use Fdsn\DataStructure\Depth;
use Fdsn\DataStructure\DepthRange;
use Fdsn\DataStructure\Epicenter;
use Fdsn\DataStructure\LatLon;
use Fdsn\DataStructure\LatLonRange;
use Fdsn\DataStructure\Location;
use Fdsn\DataStructure\Magnitude;
use Fdsn\DataStructure\MagnitudeRange;
use Fdsn\DataStructure\Quake;
use Fdsn\DataStructure\Radius;
use Fdsn\DataStructure\RadiusRange;

/**
 * PHP library to access FDSN Webservices and request Event (earthquake) information in text format
 * @see	https://www.fdsn.org/webservices/fdsnws-event-1.2.pdf FDSN official documentation
 *
 * @param string 	$format 	Accepted output kind format
 * @param string 	$user		Set your app name
 * @param ?string 	$fdns_server	Fdns webservice domain name (default: webservices.ms.ingv.it)
 */
class Event implements IteratorAggregate {
	private string $format = 'text';
	private string $user;
	private string $fdns_server;
	private string $webserviceFullPath;

	/* Search by eventId */	
	private int $eventId;

	/* Search by bounding box */	
	private LatLonRange 	$square;

	/* Search by magnitude */	
	private MagnitudeRange 	$magnitudeRange;

	/* Search by datetime */	
	private DateTimeRange	$dateTimeRange;

	/* Search by depth */	
	private DepthRange 	$depthRange;

	/* Search by radius */	
	private LatLon	$latlon;
	private RadiusRange $radiusRange;
	

	private string $url;
	private string $eventList;
	private array $events = array(); //array of Fdsn\DataStructure\Quake
	private Quake $event;
	private int $counter = 0;

	function __construct( string $format, string $user, ?string $fdns_server){

		if( $this->formatIsValid($format) )
			$this->format = $format; //otherwise it has default value

		$this->fdnsServer =  empty($fdns_server) ? Settings::defaultFdsnServer : $fdns_server;

		$this->webserviceFullPath = sprintf(Settings::basePath,
			$this->fdnsServer
			);

		$this->user = $user;

		if( ! $this->fdsnSupportedVersion())
			throw new \RuntimeException("Unsupported FDSN version");
	}
	
	/**
	 * Add filter based on ID Event (see https://terremoti.ingv.it/ to get event ids)
	 * @param int 		$eventId 	Unique event ID
	 */
	public function addFilterByEventId(int $eventId):void { $this->eventId = $eventId; }

	/**
	 * Add filter based on datetime range
	 * @param \Fdsn\DataStructure\DateTimeRange $dateTimeRange 	Datetime min -> Datetime max (default value is TODAY)
	 */
	public function addFilterByDateTimeRange(DateTimeRange  $dateTimeRange):void { $this->dateTimeRange = $dateTimeRange; }

	/**
	 * Add filter based on geographical selection
	 * @param \Fdsn\DataStructure\LatLonRange $square 	Lat/Lon min -> Lat/Lon max
	 */
	public function addFilterByBoundingBox(LatLonRange $square):void { $this->square = $square; }

	/**
	 * Add filter based on magnitude range
	 * @param \Fdsn\DataStructure\MagnitudeRange 	$magnitudeRange 	Magnitude min -> Magnitude max
	 */
	public function addFilterByMagnitudeRange(MagnitudeRange $magnitudeRange):void { $this->magnitudeRange = $magnitudeRange; }

	/**
	 * Add filter based on depth range
	 * @param \Fdsn\DataStructure\DepthRange $depthRange 	Depth min -> Depth max
	 */
	public function addFilterByDepthRange(DepthRange  $depthRange):void { $this->depthRange  = $depthRange; }

	/**
	 * Add filter based on latlon->radius
	 * @param \Fdsn\DataStructure\LatLon		$latlon		lat/lon as coordinate to start search
	 * @param \Fdsn\DataStructure\RadiusRange 	$radiusRange	radius min->max to search ( valid range [0:180] for both radius )
	 */
	public function addFilterByRadiusRange(LatLon $latlon, RadiusRange $radiusRange ):void { $this->latlon = $latlon;  $this->radiusRange = $radiusRange; }

	/**
	 * Apply all filter and DO the request to the Official ONT Webservice
	 * @return int 		Return the number of elems found
	 */
	public function fetch():int{
		$this->composeURL();

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $this->url,
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> 1,
					CURLOPT_TIMEOUT		=> 60
					)
				);
		$response = curl_exec($curlSession);
		curl_close($curlSession);

		$this->parse($response);

		return $this->counter;
	}
	
	/**
	 * Return the number of elems found
	 * @return int 		The number of elems found
	 */
	public function getNumRows():int { return $this->counter; }

	/**
	 * Iterate over events found
	 * @return array 	array of \Fdsn\DataStructure\Quake obj
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->events); }

	/**
	 * Get single event found, if only one event was found
	 * (filtering by eventId or when only one event is found by fetch() )
	 * @return \Fdsn\DataStructure\Quake 	Quake obj
	 */
        public function getSingleElemFound():Quake{ return $this->event; }

	/**
	 * Check if format set is valid
	 * @return bool		format is valid?
	 */
	private function formatIsValid(string $format):bool{ 
		return preg_match('/text/', $format); 

		//TODO: 2024-08-13: xml format is still not supported, but i'll fix
		//return preg_match('/(text|xml)/', $format);  
	}

	/**
	 * Check if FDSN server version is supported
	 */
	private function fdsnSupportedVersion():bool{
		$version = new Version($this->fdnsServer);
                return preg_match('/' . Settings::fdsnSupportedVersion . '/', $version->version());

	}

	/**
	 * Compose URL before do the request
	 */
	private function composeURL():void{
		$parameters = array();
		$parameters[] = sprintf("format=%s", $this->format);
		$parameters[] = sprintf("user=%s", $this->user);

		if( isset($this->eventId) && ! is_null($this->eventId) )
			$parameters[] = sprintf("eventid=%s", $this->eventId);

		if( isset($this->dateTimeRange) && ! is_null($this->dateTimeRange))
			$parameters[] = sprintf("starttime=%s&endtime=%s", 
						$this->dateTimeRange->startDateTime(),
						$this->dateTimeRange->endDateTime());

		if( isset($this->square) && ! is_null($this->square) )
			$parameters[] = sprintf("minlat=%f&maxlat=%f&minlon=%f&maxlon=%f",
				$this->square->min()->lat(),
				$this->square->max()->lat(),
				$this->square->min()->lon(),
				$this->square->max()->lon());

		if( isset($this->magnitudeRange) && ! is_null($this->magnitudeRange) )
			$parameters[] = sprintf("minmag=%f&maxmag=%f",
				$this->magnitudeRange->min()->value(),
				$this->magnitudeRange->max()->value());

		if( isset($this->depthRange) &&  ! is_null($this->depthRange) )
			$parameters[] = sprintf("mindepth=%f&maxdepth=%f",
				$this->depthRange->min()->value(),
				$this->depthRange->max()->value());

		if(  ( isset($this->latlon) && ! is_null($this->latlon) ) && ( isset($this->radiusRange)) && ! is_null($this->radiusRange) )
			$parameters[] = sprintf("latitude=%f&longitude=%f&minradius=%.2f&maxradius=%.2f",
				$this->latlon->lat(), $this->latlon->lon(),
				$this->radiusRange->min()->value(), $this->radiusRange->max()->value());

		$this->url = sprintf('%s/query?%s', $this->webserviceFullPath, implode('&', $parameters));
		//error_log($this->url);
	}

	/**
	 * Loop over found elems found (in text format) and convert the to \Fdsn\DataStructure\Quake obj
	 */
	private function parse(string $response):int {
		date_default_timezone_set('UTC');

		$events = preg_split('/\R/',  $response);

		foreach($events as $details){
			if(empty($details) || '#' == $details[0]){
				//error_log('[INF] no data in this event line');
				continue;
			}

			list($ADSIdEvent, $UTCTime, $Lat, $Lon, $Depth, $Author, $Catalog, $Contributor, $ContributorID, $MagPrefType, $MagPrefValue, $MagAuthor, $LocationName) = preg_split('/\|/', $details);
			$this->events[] = new Quake(
				$ADSIdEvent,
				new \DateTime($UTCTime),
				new Location($LocationName),
				new Magnitude($MagPrefType, $MagPrefValue),
				new Epicenter(new LatLon($Lat, $Lon), new Depth($Depth)),
				new Author($Author)
			);

		}
		$this->counter = count($this->events);
		
		if(1 == $this->counter)
			$this->event = $this->events[0];

		return $this->counter;
	}

	
}
?>
