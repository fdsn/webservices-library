<?php
namespace Fdsn\Webservices;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

use Fdsn\DataStructure\Catalog as DS_Catalog;

/**
 * PHP library to access FDSN Webservices and request Catalogs
 *
 * @param ?string 	$fdsn_server	Fdns webservice domain name (default: webservices.ms.ingv.it)
 */
class Catalogs implements IteratorAggregate {

	private string $fdsn_server;
	private string $webserviceFullPath;

	private string $url;

	private string $catalogsXmlString;
	private \DOMDocument $catalogsDOM;
	private array $catalogsArray;
	private int $counter = 0;

	function __construct( ?string $fdsn_server){

		$this->fdsnServer =  empty($fdsn_server) ? Settings::defaultFdsnServer : $fdsn_server;

		$this->webserviceFullPath = sprintf(Settings::basePath,
			$this->fdsnServer
			);
	}
	
	/**
	 * Return the number of elems found
	 * @return int 		The number of elems found
	 */
	public function getNumRows():int { return $this->counter; }

	/**
	 * Iterate over catalogis found
	 * @return array 	array of \Fdsn\DataStructure\Catalog obj
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->catalogsArray); }

	/**
	 * Get RAW Catalogs XML String
	 * @return string	Get RAW Catalogs XML String
	 */
        public function rawXMLCatalogs():string { return $this->catalogsXmlString; }

	/**
	 * Get Catalogs parsed in a DOM Document
	 * @return \DOMDocument Get Catalogs parsed in a DOMDocument
	 */
        public function DOMCatalogs():\DOMDocument{ return $this->catalogsDOM; }

	/**
	 * Check if FDSN server version is supported
	 */
	private function fdsnSupportedVersion():bool{
		$version = new Version($this->fdsnServer);
                return preg_match('/' . Settings::fdsnSupportedVersion . '/', $version->version());

	}

	/**
	 * Fetch catalogs (xml document) from selected FDSN server
	 * 
	 * @return int number of catalogs found
	 */
	public function fetch():int{
		$catalogsUrl = $this->webserviceFullPath . '/catalogs';

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $catalogsUrl,
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> 1,
					CURLOPT_TIMEOUT		=> 60
					)
				);
		$xmlResponse = trim(curl_exec($curlSession));
		curl_close($curlSession);

		$this->catalogsXmlString = ( $this->parse($xmlResponse) > 0 ) ? $xmlResponse : '';

		return $this->counter;
	}

	/**
	 * Parse XML Catalogs and store it into a DOM Document AND all values found into an Iterable array
	 * 
 	 * @param string $xmlResponse xml document got by API request
	 *
	 * @return int number of catalogs found
	 */
	private function parse(string $xmlResponse):int{
		$this->catalogsDOM = new \DOMDocument('1.0', 'UTF-8');
		$this->catalogsDOM->loadXML($xmlResponse);

		$catalogs = $this->catalogsDOM->getElementsByTagName('Catalog');
		foreach($catalogs as $name)
			$this->catalogsArray[] = new DS_Catalog(trim($name->nodeValue));
	
		$this->counter = count($this->catalogsArray);

		return $this->counter;
	}

	
}
?>
