<?php
namespace Fdsn\Webservices;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

use Fdsn\DataStructure\Catalog as DS_Catalog;

/**
 * PHP library to access FDSN Webservices and request Catalogs
 *
 * @param ?string 	$fdns_server	Fdns webservice domain name (default: webservices.ms.ingv.it)
 */
class Catalogs implements IteratorAggregate {
	const fdsnSupportedVersion = '^1.60';

	private string $fdns_server;
	private string $webserviceFullPath;

	private string $url;

	private string $catalogsXMLString;
	private \DOMDocument $catalogsDOM;
	private array $catalogsArray;
	private int $counter = 0;

	function __construct( ?string $fdns_server){

		$this->fdnsServer =  empty($fdns_server) ? Settings::defaultFdsnServer : $fdns_server;

		$this->webserviceFullPath = sprintf(Settings::basePath,
			$this->fdnsServer
			);

		if( ! $this->fdsnSupportedVersion())
			throw new \RuntimeException("Unsupported FDSN version");

		if ( '' == $this->retrieveCatalogs() )
			throw new \ValueError('Catalogs is empty');

		$this->parseCatalogs();
	}
	
	/**
	 * Return the number of elems found
	 * @return int 		The number of elems found
	 */
	public function getNumRows():int { return $this->counter; }

	/**
	 * Iterate over catalogis found
	 * @return array 	array of \Fdsn\DataStructure\Catalog obj
	 */
        public function getIterator():Traversable { return new ArrayIterator($this->catalogsArray); }

	/**
	 * Get RAW Catalogs XML String
	 * @return string	Get RAW Catalogs XML String
	 */
        public function rawXMLCatalogs():string { return $this->catalogsXMLString; }

	/**
	 * Get Catalogs parsed in a DOM Document
	 * @return \DOMDocument Get Catalogs parsed in a DOMDocument
	 */
        public function DOMCatalogs():\DOMDocument{ return $this->catalogsDOM; }

	/**
	 * Check if FDSN server version is supported
	 */
	private function fdsnSupportedVersion():bool{
		$version = new Version($this->fdnsServer);
                return preg_match('/' . self::fdsnSupportedVersion . '/', $version->version());

	}

	/**
	 * Connect to FDSN server to retrieve working version
	 * @return string FDSN running version found (trimmed by newline)
	 */
	private function retrieveCatalogs():string {
		$catalogsUrl = $this->webserviceFullPath . '/catalogs';

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $catalogsUrl,
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> 1,
					CURLOPT_TIMEOUT		=> 60
					)
				);
		$this->catalogsXMLString = trim(curl_exec($curlSession));
		curl_close($curlSession);

		return $this->catalogsXMLString;
	}

	/**
	 * Parse XML Catalogs and store it into a DOM Document AND all values found into an Iterable array
	 */
	private function parseCatalogs():void{
		$this->catalogsDOM = new \DOMDocument('1.0', 'UTF-8');
		$this->catalogsDOM->loadXML($this->catalogsXMLString);

		$catalogs = $this->catalogsDOM->getElementsByTagName('Catalog');
		foreach($catalogs as $name)
			$this->catalogsArray[] = new DS_Catalog(trim($name->nodeValue));
	
		$this->counter = count($this->catalogsArray);
	}

	
}
?>
