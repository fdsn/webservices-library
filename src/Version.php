<?php
namespace Fdsn\Webservices;

/**
 * PHP library to access FDSN Webservices and request its Version 
 *
 * @param ?string 	$fdns_server	Optional. Fdns webservice domain name (default: webservices.ms.ingv.it)
 */
class Version {

	private string $webserviceFullPath;

	private string $url;
	private string $version = '';

	function __construct( ?string $fdns_server){

		$this->webserviceFullPath = sprintf(Settings::basePath, 
			empty($fdns_server) ? Settings::defaultFdsnServer: $fdns_server
			);

		if ( empty( $this->fetch() ) )
			throw new \ValueError('FDSN Version is empty');
		
	}

	/**
	 * Get FDSN running version
	 * @return string FDSN running version (trimmed by newline)
	 */
	public function version():string{ return $this->version; }
	
	/**
	 * Fetch running version from FDSN server 
	 * @return string FDSN running version found (trimmed by newline)
	 */
	private function fetch():string {
		$versionUrl = $this->webserviceFullPath . '/version';

		$curlSession = curl_init();
		curl_setopt_array($curlSession, array(
					CURLOPT_URL 		=> $versionUrl,
					CURLOPT_HEADER 		=> false,
					CURLOPT_CUSTOMREQUEST	=> 'GET',
					CURLOPT_RETURNTRANSFER	=> 1,
					CURLOPT_TIMEOUT		=> 60
					)
				);
		$this->version = trim(curl_exec($curlSession));
		curl_close($curlSession);
	
		//error_log('FDSN running version: ' . $this->version);	
		return $this->version;
	}

}
?>
